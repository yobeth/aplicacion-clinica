package com.centrum.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.centrum.entity.ConsultaExamen;

@Repository
public interface IConsultaExamenDAO extends JpaRepository<ConsultaExamen, Integer>{
	@Transactional
	@Modifying
	@Query(value = "insert into consulta_examen(id_consulta, id_examen) values(:idConsulta, :idExamen", nativeQuery = true)
	int registrar(@Param("idConsulta") Integer idConsulta, @Param("idExamen") Integer idExamen);
}
