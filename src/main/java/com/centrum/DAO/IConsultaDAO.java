package com.centrum.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrum.entity.Consulta;

@Repository
public interface IConsultaDAO extends JpaRepository<Consulta, Integer> {

}
