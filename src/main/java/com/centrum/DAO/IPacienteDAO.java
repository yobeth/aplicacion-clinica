package com.centrum.DAO;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrum.entity.Paciente;



@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, Integer>{

}
