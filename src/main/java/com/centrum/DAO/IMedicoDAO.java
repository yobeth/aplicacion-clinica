package com.centrum.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.centrum.entity.Medico;

@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer>{

}
