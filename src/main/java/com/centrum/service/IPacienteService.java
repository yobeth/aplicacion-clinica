package com.centrum.service;

import java.util.List;

import com.centrum.entity.Paciente;

public interface IPacienteService {
	void registrar(Paciente paciente);
	void editar (Paciente paciente);
	List<Paciente> listar (); 
}
