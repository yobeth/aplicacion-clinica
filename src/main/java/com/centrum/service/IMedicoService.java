package com.centrum.service;

import java.util.List;

import com.centrum.entity.Medico;

public interface IMedicoService {
	void registrar (Medico medico);
	void editar (Medico medico);
	List<Medico> listar();

}
