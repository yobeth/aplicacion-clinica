package com.centrum.service;

import java.util.List;

import com.centrum.entity.Consulta;

public interface IConsultaService {
	void registrar (Consulta consulta);
	void editar (Consulta consulta);
	List<Consulta> listar ();

}
