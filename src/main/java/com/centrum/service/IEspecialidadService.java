package com.centrum.service;

import java.util.List;

import com.centrum.entity.Especialidad;

public interface IEspecialidadService {
	void registrar (Especialidad especialidad);
	void editar (Especialidad especialidad);
	List<Especialidad> listar ();

}
