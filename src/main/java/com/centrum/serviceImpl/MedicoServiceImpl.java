package com.centrum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IMedicoDAO;
import com.centrum.entity.Medico;
import com.centrum.service.IMedicoService;

@Service
public class MedicoServiceImpl implements IMedicoService{

	@Autowired
	private IMedicoDAO iMedicoDAO;
	
	@Override
	public void registrar(Medico medico) {
		iMedicoDAO.save(medico);
		
	}

	@Override
	public void editar(Medico medico) {
		iMedicoDAO.save(medico);
		
	}

	@Override
	public List<Medico> listar() {
		// TODO Auto-generated method stub
		return iMedicoDAO.findAll();
	}

}
