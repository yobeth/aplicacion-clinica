package com.centrum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IPacienteDAO;
import com.centrum.entity.Paciente;
import com.centrum.service.IPacienteService;

@Service
public class PacienteServiceImpl implements IPacienteService{

	@Autowired
	private IPacienteDAO iPacienteDAO;
	
	@Override
	public void registrar(Paciente paciente) {
		iPacienteDAO.save(paciente);
	}

	@Override
	public void editar(Paciente paciente) {
		iPacienteDAO.save(paciente);
		
	}

	@Override
	public List<Paciente> listar() {
		
		return iPacienteDAO.findAll();
	}

}
