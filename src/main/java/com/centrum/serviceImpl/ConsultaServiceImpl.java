package com.centrum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IConsultaDAO;
import com.centrum.entity.Consulta;
import com.centrum.service.IConsultaService;

@Service
public class ConsultaServiceImpl implements IConsultaService{

	@Autowired
	private IConsultaDAO iConsultaDAO;
	
	@Override
	public void registrar(Consulta consulta) {
		iConsultaDAO.save(consulta);
		
	}

	@Override
	public void editar(Consulta consulta) {
		iConsultaDAO.save(consulta);
		
	}

	@Override
	public List<Consulta> listar() {
		// TODO Auto-generated method stub
		return iConsultaDAO.findAll();
	}

}
