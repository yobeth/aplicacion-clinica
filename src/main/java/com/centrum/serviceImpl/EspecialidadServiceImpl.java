package com.centrum.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.centrum.DAO.IEspecialidadDAO;
import com.centrum.entity.Especialidad;
import com.centrum.service.IEspecialidadService;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

	@Autowired
	private IEspecialidadDAO iEspecialidadDAO;
	
	@Override
	public void registrar(Especialidad especialidad) {
		iEspecialidadDAO.save(especialidad);
		
	}

	@Override
	public void editar(Especialidad especialidad) {
	iEspecialidadDAO.save(especialidad);
		
	}

	@Override
	public List<Especialidad> listar() {
		// TODO Auto-generated method stub
		return iEspecialidadDAO.findAll();
	}

}
