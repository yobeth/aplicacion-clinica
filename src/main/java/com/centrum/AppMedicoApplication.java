package com.centrum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMedicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppMedicoApplication.class, args);
	}

}
