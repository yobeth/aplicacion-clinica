package com.centrum.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="medico")
public class Medico {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int idMedico;
	
	@Column(name="nombres", nullable=false, length=70)
	private String nombres;
	
	@Column(name="apellidos", nullable=false, length=70)
	private String apellidos;
	
	@Column(name="CPM", nullable=false, length=12)
	private String CPM;

	public int getIdMedico() {
		return idMedico;
	}

	public void setIdMedico(int idMedico) {
		this.idMedico = idMedico;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCPM() {
		return CPM;
	}

	public void setCPM(String cPM) {
		CPM = cPM;
	}
	

	
}
